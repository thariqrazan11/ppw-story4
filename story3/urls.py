from django.urls import include, path
from .views import s3Index, s3Resume, time

urlpatterns = [
    path('', s3Index, name='s3Index'),
    path('resume/', s3Resume, name='s3Resume'),
    path('<int:timezone>/', time, name='time'),
]
