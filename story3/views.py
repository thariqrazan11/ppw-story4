from django.shortcuts import render
from django.http import HttpResponse
import datetime
# Create your views here.

def s3Index(request):
    return render(request,"story3_index.html")

def s3Resume(request):
    return render(request,"story3_resume.html")

def time(request, timezone='0'):
    time = datetime.datetime.now() +datetime.timedelta(hours=int(timezone))
    return render(request,"time.html", {'time':time})